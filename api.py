import requests
from requests.auth import HTTPBasicAuth
import csv
import random
import string
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from pathlib import Path
import os
from dotenv import load_dotenv

dotenv_path = Path('./.env')
load_dotenv(dotenv_path=dotenv_path)
HOST = os.getenv('HOST')
API_KEY = os.getenv('API_KEY')
EMAIL_USERNAME = os.getenv('EMAIL_USERNAME')
EMAIL_PASSWORD = os.getenv('EMAIL_PASSWORD')
SMTP_SERVER = os.getenv('SMTP_SERVER')
SMTP_PORT = os.getenv('SMTP_PORT')

api_key = API_KEY
url = HOST+"/api/v3/users"

headers = {
    "Content-Type": "application/json",
}

# Nextcloud csv users information extract path
csv_file_path = './user.csv'
output_file_path = 'output_credentials.txt'

def send_email(receiver_email, subject, body):
    # Set up the MIME
    message = MIMEMultipart()
    message['From'] = EMAIL_USERNAME
    message['To'] = receiver_email
    message['Subject'] = subject

    # Attach the body of the email
    message.attach(MIMEText(body, 'plain'))

    # Connect to the SMTP server and send the email
    with smtplib.SMTP(SMTP_SERVER, SMTP_PORT) as server:
        server.starttls()
        server.login(EMAIL_USERNAME, EMAIL_PASSWORD)
        server.sendmail(EMAIL_USERNAME, receiver_email, message.as_string())

try:
    with open(csv_file_path, newline='') as csvfile, open(output_file_path, 'w') as output_file:
        reader = csv.DictReader(csvfile)
        for csv_row in reader:
            # Generate a random password of 10 characters
            password = ''.join(random.choices(string.ascii_letters + string.digits, k=10))

            # Map elements from CSV to the payload structure
            payload = {
                "login": csv_row["email"],
                "password": password,
                "firstName": csv_row["firstname"],
                "lastName": csv_row["lastname"],
                "email": csv_row["email"],
                "admin": False,
                "status": "active",
                "language": "en",
            }
            
            # API request to create open project users
            response = requests.post(
                url,
                json=payload,
                headers=headers,
                auth=HTTPBasicAuth("apikey", api_key),
            )

            # Print the response details for each API call
            print(f"Status Code: {response.status_code}")
            print("Response Body:", response.json())
            response.raise_for_status()

            # Write email and password to the output file
            output_file.write(f"Email: {csv_row['email']}, Password: {password}\n")

            # Send an email to the user with login and password
            subject = "Your Login and Password Information"
            body = f"Hello {csv_row['firstname']},\n\nYour login information to connect to open project website ({HOST}) are the following :\nEmail: {csv_row['email']}\nPassword: {password}\n\nBest regards,\nUNITA"
            print(subject)
            print(body)
            # send_email(csv_row.['email', ''), subject, body)

except FileNotFoundError:
    print("Error: CSV file not found.")
except requests.exceptions.RequestException as e:
    print(f"Error: {e}")
except smtplib.SMTPException as e:
    print(f"Error sending email: {e}")


